# Python Girls - Main website

The python girls is a non profit organization providing a support network and one day workshops to encourage more active female developers.

The project currently runs in Ukraine, with plans to grow to other parts of Europe.


# Python girls Ukraine (Project brief)

Software engineering is one of the few ways to earn an excellent salary in Ukraine. Salaries start at $500 USD for beginners, $1500 for intermediate and $3500+ for senior positions.

Approximately 15% of younger men work in a technical field and a further 15% work directly with software. However less than 1% of young women write code and even fewer are experienced programmers.

The python girls Ukraine project aims to run support groups and workshops to actively encourage and promote the development of female programmers.

Our initial one day workshops will provide a practical, hands on introduction to programming, which will show non-developers how writing software can be useful in both their professional and personal lives.

This will be supported by informal 'code and coffee' meetups where people can come together to share, discuss and learn from each other about code that they are actively working on.

The python girls project is designed to support existing programming schools and services, rather than to compete with them. 
There are fantastic sources on the Internet to learn programing, the hard part is doing it yourself.

Our goal is to provide support through community driven regular meetups and focused workshops.


The goal would be to have 50 people for an afternoon e.g. 1pm - 6pm, with a networking drinks later in the evening.
We want the workshop to be long enough for people to get a taste of what programing is like, but not to exhaust them.
The networking drinks afterwards are important - we want to form a strong network among people so that they will support each other as they study.

# Technical

Currently this website is simple html files, served up by netlify. This is due to 

simple > complex

and the lack of a specification (i.e. we don't know at this stage of this project, whats needed website wise). This also makes it easier for graphic designers to modify/add content.

# Terminal SVG animations

Terminal SVG animations are generated using https://github.com/nbedos/termtosvg

The window frame template is used

```shell
termtosvg -t window_frame
```

# Licence

Python Girls:

Free for personal and commercial use under the CCA 3.0 license

info@pythongirls.com

Images:

https://www.pexels.com/
https://unsplash.com/

Theme based off:

Editorial by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)